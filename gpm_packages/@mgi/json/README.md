> JSON is lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate.
>
> -- <cite>[JSON.org](https://www.json.org/)</cite>

The MGI JSON parser is optimized for speed and programming convenience. It maximizes your ability to move between JSON strings and LabVIEW data types.

This library is flexible than the built in JSON parser. It handles conversion from almost all LabVIEW data types, and allows you to more easily access data when the exaction JSON format isn't known until runtime.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
